import { FETCH_WEATHER_REQUEST, FETCH_WEATHER_SUCCESS } from '../action/index';
import { takeLatest, all, call, put } from 'redux-saga/effects';
import axios from 'axios';

const api = {
    key: "4d6ed2127e17f4853cadca34c7a7cb24",
    base: "https://api.openweathermap.org/data/2.5/"
}

function fetchWeather(city) {
    console.log("saga----", city)
    const url = `${api.base}weather?q=${city}&unitsmetric&APPID=${api.key}`
    return axios.get(url);
}

export function* fetchWeatherSaga(action) {
    console.log("fetchWeatherSaga -----", action)
    try {
        const response = yield call(fetchWeather, action.payload);
        yield put({ type: FETCH_WEATHER_SUCCESS, payload: response });
    }
    catch (err) {
        console.log(err);
    }
}

export default function* rootSaga() {
    yield all([
        yield takeLatest(FETCH_WEATHER_REQUEST, fetchWeatherSaga)
    ])
}