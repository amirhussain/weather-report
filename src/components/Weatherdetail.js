import React from 'react';
import { connect } from 'react-redux';

const WeatherDetail = (props) => {
    let weather = props.weather[0];
    console.log(weather)

    const dateBuilder = (d) => {
        let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        let day = days[d.getDay()];
        let date = d.getDate();
        let month = months[d.getMonth()];
        let year = d.getFullYear();

        return `${day} ${date} ${month} ${year}`;
    }
    return (
        <div className={(typeof weather != "undefined") ? ((Math.round(weather.main.temp - 273.15) > 16) ? 'warm' : 'app') : 'app'}>
            {(typeof weather !== "undefined") ?
                <div>
                    <div className="location-box">
                        <div className="location">{weather.name}, {weather.sys.country}</div>
                        <div className="date">{dateBuilder(new Date())}</div>
                    </div>
                    <div className="weather-box">
                        <div className="temp">
                            {Math.round(weather.main.temp - 273.15)}&#176;C
                    </div>
                        <div className="weather">{weather.weather[0].main}</div>
                    </div>
                </div>
                : ''}
        </div>
    )
}

// Add this function:
function mapStateToProps(state) {
    return {
        weather: state
    };
}

export default connect(mapStateToProps, null)(WeatherDetail);
