import React, { useState } from 'react';
import { connect } from 'react-redux';
import '../../src/index.css'
import * as actions from '../action/index';
import Weatherdetail from './Weatherdetail';

// const api = {
//   key: "4d6ed2127e17f4853cadca34c7a7cb24",
//   base: "https://api.openweathermap.org/data/2.5/"
// }
let weatherReport = {};

function App(props) {
  const [query, setQuery] = useState('');
  const [weather, setWeather] = useState({});

  const search = evt => {
    if (evt.key === "Enter") {
      console.log(query)
      props.fetchWeatherCity(query);
      setWeather(weatherReport)
      // fetch(`${api.base}weather?q=${query}&unitsmetric&APPID=${api.key}`)
      //   .then(res => res.json())
      //   .then(result => {
      //     setWeather(result);
      //     setQuery('');
      //     console.log(result);
      //   });
    }
  }



  return (
    <div >
      <main>
        <div className="search-box">
          <input
            type="text"
            className="search-bar"
            placeholder="Search..."
            onChange={e => setQuery(e.target.value)}
            value={query}
            onKeyPress={search}
          />
        </div>
        <Weatherdetail />
      </main>
    </div>
  );
}

function mapStateToProps(state) {
  console.log(state)
  weatherReport = state
}

export default connect(null, actions)(App);
