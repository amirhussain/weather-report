export const FETCH_WEATHER_REQUEST = "FETCH_WEATHER_REQUEST",
    FETCH_WEATHER_SUCCESS = "FETCH_WEATHER_SUCCESS";

export function fetchWeatherCity(city) {
    console.log(city)
    return {
        type: FETCH_WEATHER_REQUEST,
        payload: city
    }
}
