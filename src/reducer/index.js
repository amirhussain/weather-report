import { FETCH_WEATHER_SUCCESS } from '../action/index';

export default (state = [], action) => {
    console.log("reducer ....", action)
    switch (action.type) {
        case FETCH_WEATHER_SUCCESS:
            return [action.payload.data, ...state];
        default:
            return state;
    }

}